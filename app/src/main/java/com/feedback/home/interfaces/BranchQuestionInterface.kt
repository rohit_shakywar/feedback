package com.feedback.home.interfaces

import com.google.android.material.textfield.TextInputEditText

interface BranchQuestionInterface {
    fun addQuestion(position:Int,questionId:String)
    fun addAnswer(position:Int,answerId:Int)
    fun addComment(position:Int,comment:String)
    fun addRating(position:Int,rating:Int,ratingChanged:Boolean)
}