package com.feedback.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.feedback.R
import com.feedback.databinding.LayoutSessionBinding
import com.feedback.model.RequestFeedBack.branchid
import com.feedback.model.RequestFeedBack.sessionid
import com.feedback.model.Session

class AdapterSession(
    val context: Context,
    val list: List<Session>
) :
    RecyclerView.Adapter<AdapterSession.ViewHolder>() {
    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutSessionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            binding.tvTitle.text = list[position].sessionName

            if (selectedPosition == position) {
                binding.root.isSelected = true
                binding.mainCard.setCardBackgroundColor(ContextCompat.getColor(context, R.color.purple_700))
                binding.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
                branchid= list[position].branchId
                sessionid= list[position].sessionId
//                binding.tvRecommend.setTextColor(ContextCompat.getColor(context, R.color.black))
//                binding.tvRecommend.setBackgroundColor(ContextCompat.getColor(context, R.color.purple_500))

            } else {
                binding.root.isSelected = false
                binding.mainCard.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                binding.tvTitle.setTextColor(ContextCompat.getColor(context, R.color.iconColor))
//                binding.tvRecommend.setTextColor(ContextCompat.getColor(context, R.color.white))
//                binding.tvRecommend.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))

            }
            binding.root.setOnClickListener {

                if (selectedPosition >= 0)
                    notifyItemChanged(selectedPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition)

            }

        }
    }

    override fun getItemCount(): Int {

        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = LayoutSessionBinding.bind(itemView)

    }
}