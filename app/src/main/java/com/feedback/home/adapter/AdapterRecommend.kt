package com.feedback.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.feedback.R
import com.feedback.databinding.LayoutRecommendBinding
import com.feedback.home.interfaces.BranchQuestionInterface
import com.feedback.model.ModelRecommend

class AdapterRecommend(
    val context: Context,
    val list: ArrayList<ModelRecommend>,
    var branchQuestionInterface: BranchQuestionInterface,
    var counter: Int
) :
    RecyclerView.Adapter<AdapterRecommend.ViewHolder>() {
    private var selectedPosition = -1
    private var isFirstTime = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutRecommendBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            binding.tvRecommend.text = list[position].name

            if (list[position].isDefault != "0" && isFirstTime) {
                selectedPosition = list[position].isDefault?.toInt() - 1
                isFirstTime = false
            }

            if (selectedPosition == position) {
                branchQuestionInterface.addAnswer(counter,list[position].name.toInt())
                binding.root.isSelected = true
                binding.tvRecommend.setTextColor(ContextCompat.getColor(context, R.color.black))
                binding.tvRecommend.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        R.color.purple_500
                    )
                )
            } else {
                binding.root.isSelected = false
                binding.tvRecommend.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.tvRecommend.setBackgroundColor(
                    ContextCompat.getColor(
                        context,
                        android.R.color.transparent
                    )
                )
            }


            binding.root.setOnClickListener {

                if (selectedPosition >= 0)
                    notifyItemChanged(selectedPosition)
                selectedPosition = adapterPosition
                notifyItemChanged(selectedPosition)


            }
        }

    }

    override fun getItemCount(): Int {

        return list.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = LayoutRecommendBinding.bind(itemView)

    }
}