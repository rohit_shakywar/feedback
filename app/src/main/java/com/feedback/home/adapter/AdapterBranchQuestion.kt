package com.feedback.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.feedback.databinding.LayoutBranchQuestionBinding
import com.feedback.home.interfaces.BranchQuestionInterface
import com.feedback.model.ModelRecommend
import com.feedback.model.Question
import java.util.ArrayList

class AdapterBranchQuestion(
    val context: Context,
    val counter: Int,
    val data: ArrayList<Question>,
    val branchQuestionInterface: BranchQuestionInterface
) :
    RecyclerView.Adapter<AdapterBranchQuestion.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            LayoutBranchQuestionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {

            if (data[counter].questionType == "1") {
                branchQuestionInterface.addQuestion(counter, data[position].id)
                branchQuestionInterface.addAnswer(counter, data[counter].isDefault.toInt())
                branchQuestionInterface.addRating(counter, data[counter].isDefault.toInt(),false)
                binding.ratingBar.visibility = View.VISIBLE
//                binding.ratingBar.rating = data[counter].isDefault?.toFloat()
            }

            if (data[counter].questionType == "2") {
                branchQuestionInterface.addQuestion(counter, data[position].id)
                binding.recyclerView.visibility = View.VISIBLE
                val list = ArrayList<ModelRecommend>()

                for (i in 1..10) {
                    if (data[counter].isDefault == i.toString()) {
                        list.add(ModelRecommend(i.toString(), data[counter].isDefault))

                    } else {
                        list.add(ModelRecommend(i.toString(), "0"))
                    }
                }
                binding.recyclerView.layoutManager =
                    GridLayoutManager(context, 10, GridLayoutManager.VERTICAL, false)
                binding.recyclerView.adapter =
                    AdapterRecommend(context, list, branchQuestionInterface,counter)

            }

            if (data[counter].questionType == "3") {
                binding.etComment.visibility = View.VISIBLE
                binding.etComment.addTextChangedListener {
                    if (it?.isNotEmpty()!!){
                    branchQuestionInterface.addComment(counter,binding.etComment.text.toString().trim())
                }}
            }

            binding.ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
                branchQuestionInterface.addAnswer(counter, rating.toInt())
                branchQuestionInterface.addRating(counter, rating.toInt(),true)

            }

        }

    }

    override fun getItemCount(): Int {

        return 1
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = LayoutBranchQuestionBinding.bind(itemView)

    }
}