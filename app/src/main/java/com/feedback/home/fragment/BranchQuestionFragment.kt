package com.feedback.home.fragment

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.feedback.R
import com.feedback.databinding.FragmentBranchQuestionBinding
import com.feedback.home.adapter.AdapterBranchQuestion
import com.feedback.home.interfaces.BranchQuestionInterface
import com.feedback.model.ModelSaveFeedBack
import com.feedback.model.Question
import com.feedback.model.RequestFeedBack
import com.feedback.model.RequestFeedBack.answerId
import com.feedback.model.RequestFeedBack.branchid
import com.feedback.model.RequestFeedBack.comments
import com.feedback.model.RequestFeedBack.questionsId
import com.feedback.model.RequestFeedBack.ratingList
import com.feedback.model.RequestFeedBack.sessionid
import com.feedback.model.RequestFeedBack.subscribe
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.storage.MyPreference
import com.feedback.util.Utils.showSnackBar
import com.google.android.material.textfield.TextInputEditText
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList


class BranchQuestionFragment : Fragment(), BranchQuestionInterface {

    private lateinit var binding: FragmentBranchQuestionBinding
    private val progressBar = CustomProgressDialog()
    private var counter = 0
    private lateinit var data: ArrayList<Question>
    private var status = false
    private lateinit var answerList: ArrayList<Int>
    private lateinit var questionList: ArrayList<String>
    private lateinit var ratingList: ArrayList<Int>
    private var totalQuestion = 0
    private var sId = 0
    private lateinit var branchQuestionInterface: BranchQuestionInterface
    private lateinit var myPreference: MyPreference
    private var ratingPosition = -1
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBranchQuestionBinding.inflate(inflater, container, false)
        initInstance()
        return binding.root
    }

    private fun initInstance() {
        data = ArrayList()
        answerList = ArrayList()
        questionList = ArrayList()
        ratingList = ArrayList()
        sId = sessionid?.toInt()!!
        myPreference = MyPreference(requireContext())
        data = myPreference.getData("login")?.data?.session!![sId - 1].questions


        branchQuestionInterface = this
        status = myPreference.getUserDetails("userDetails").status

        initListener()
    }

    private fun initListener() {

        binding.btnNext.setOnClickListener {
            if (binding.btnNext.text.equals("Submit")) {
                subscribe = if (binding.checkbox.isChecked) {
                    "Yes"
                } else {
                    "No"
                }
                if (status) {
                    saveFeedBack()
                } else {
                    answerId = answerList
                    questionsId = questionList
                    RequestFeedBack.ratingList = ratingList
                    findNavController().navigate(R.id.contactFragment)
                }
            } else {

                if (counter != data.size - 1) {
//                    binding.btnBack.visibility = View.VISIBLE
                    counter++
                    binding.tvTitle.text = data[counter].title
                    if (counter == data.size - 1) {
                        binding.btnNext.text = "Submit"
                        binding.checkbox.visibility=View.VISIBLE
                    }
                    initSetUpView()
                }
            }

        }

        binding.btnBack.setOnClickListener {

            if (counter != 0) {
                counter--
                binding.tvTitle.text = data[counter].title
                binding.btnNext.text = "Next"
                initSetUpView()
                if (counter == 0) {
                    binding.btnBack.visibility = View.GONE
                }
            }

        }
        binding.tvTitle.text = data[counter].title
        initSetUpView()

//        getData()
    }

    private fun saveFeedBack() {
        progressBar.show(requireContext())
        val rate = ratingList.sum() / ratingList.size
        val question = questionList?.joinToString { it }
        val answer = answerList?.joinToString { it.toString() }


        val login = MyPreference(requireContext()).getData("login")?.data
        val userDetails = MyPreference(requireContext()).getUserDetails("userDetails")?.data
        val body = MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart("branchId", "$branchid")
            .addFormDataPart("userId", "${login?.id}")
            .addFormDataPart("sessionId", "$sessionid")
            .addFormDataPart("rating", "$rate")
            .addFormDataPart("comments", "$comments")
            .addFormDataPart("Subscribed", "$subscribe")
            .addFormDataPart("questionsId", question)
            .addFormDataPart("answer", "$answer")
            .addFormDataPart("customerId", "${userDetails.id}")
//            .addFormDataPart("customerName", "${userDetails.name}")
//            .addFormDataPart("customerEmail", "${userDetails.email}")
//            .addFormDataPart("customerPhone", "${userDetails.phone}")
//            .addFormDataPart("customerCity", "${userDetails.city}")

        val request = body.build()

        ServiceBuilder().service.saveFeedBack(request)
            .enqueue(
                object : Callback<ModelSaveFeedBack> {
                    override fun onResponse(
                        call: Call<ModelSaveFeedBack>,
                        response: Response<ModelSaveFeedBack>
                    ) {
                        if (response.isSuccessful && response.body()?.status == true) {
                            findNavController().navigate(R.id.thankYouFragment)

                        } else {
                            showSnackBar(
                                requireContext(),
                                response.body()?.message.toString()
                            )
                        }
                        progressBar.dialog.dismiss()

                    }

                    override fun onFailure(call: Call<ModelSaveFeedBack>, t: Throwable) {
                        progressBar.dialog.dismiss()
                        showSnackBar(requireContext(), t.message.toString())
                    }
                })

    }


    private fun initSetUpView() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = AdapterBranchQuestion(
            requireContext(), counter, data, branchQuestionInterface
        )

    }

    override fun addQuestion(position: Int, questionId: String) {

        try {
            if (questionList.size != 0) {
                questionList.forEachIndexed { index, s ->
                    if (index == position) {
                        questionList.removeAt(position)
                    }
                }
            }
            questionList.add(position, questionId)
            print(questionList)
        } catch (e: ConcurrentModificationException) {
            e.printStackTrace()
        }

    }

    override fun addAnswer(position: Int, answerId: Int) {

        try {
            if (answerList.size != 0) {
                answerList.forEachIndexed { index, s ->
                    if (index == position) {
                        answerList.removeAt(position)
                    }
                }
            }
            answerList.add(position, answerId)
            print(answerList)
        } catch (e: ConcurrentModificationException) {
            e.printStackTrace()
        }

    }

    override fun addComment(position: Int, comment: String) {
        comments = comment
    }

    override fun addRating(position: Int, rating: Int, ratingChanged: Boolean) {
        ratingPosition++
        if (ratingChanged) {
            ratingPosition--
        }
        try {
            if (ratingList.size != 0) {
                ratingList.forEachIndexed { index, s ->
                    if (index == ratingPosition) {
                        ratingList.removeAt(ratingPosition)
                    }
                }
            }
            ratingList.add(ratingPosition, rating)
            print(ratingList)
        } catch (e: ConcurrentModificationException) {
            e.printStackTrace()
        }
    }
}