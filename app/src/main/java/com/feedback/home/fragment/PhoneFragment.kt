package com.feedback.home.fragment

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.feedback.R
import com.feedback.databinding.FragmentPhoneBinding
import com.feedback.model.ModelUserDetails
import com.feedback.model.RequestFeedBack.number
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.storage.MyPreference
import com.feedback.util.Utils.isNetworkAvailable
import com.feedback.util.Utils.showSnackBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PhoneFragment : Fragment() {

    private lateinit var binding: FragmentPhoneBinding
    private val progressBar = CustomProgressDialog()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPhoneBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {

        binding.btnNext?.setOnClickListener {

            if (binding.edPhone.text.toString().trim().isEmpty()) {

                showSnackBar(requireContext(), "Please enter phone number")
            } else if (!Patterns.PHONE.matcher(binding.edPhone.text.toString().trim()).matches()) {
                showSnackBar(requireContext(), "Please enter correct phone number")
            } else {
                if (isNetworkAvailable(requireContext())) {
                    number=binding.edPhone.text.toString().trim()
                    progressBar.show(requireContext(), getString(R.string.please_wait))
                    ServiceBuilder().service.getUserDetail(binding.edPhone.text.toString().trim())
                        .enqueue(object : Callback<ModelUserDetails> {
                            override fun onResponse(
                                call: Call<ModelUserDetails>,
                                response: Response<ModelUserDetails>
                            ) {
//
                                MyPreference(requireContext()).saveUserDetails("userDetails",response?.body())
                                progressBar.dialog.dismiss()
                                findNavController().navigate(R.id.branchQuestionFragment)

                            }

                            override fun onFailure(call: Call<ModelUserDetails>, t: Throwable) {
                                progressBar.dialog.dismiss()
                                showSnackBar(requireContext(), t.message.toString())
                            }
                        })
                }
            }
        }

    }
}