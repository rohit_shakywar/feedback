package com.feedback.home.fragment

import android.os.Bundle
import android.util.Patterns
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.navigateUp
import com.feedback.R
import com.feedback.databinding.FragmentContactBinding
import com.feedback.model.ModelSaveFeedBack
import com.feedback.model.RequestFeedBack
import com.feedback.model.RequestFeedBack.answerId
import com.feedback.model.RequestFeedBack.comments
import com.feedback.model.RequestFeedBack.number
import com.feedback.model.RequestFeedBack.questionsId
import com.feedback.model.RequestFeedBack.ratingList
import com.feedback.model.RequestFeedBack.subscribe
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.storage.MyPreference
import com.feedback.util.Utils
import com.feedback.util.Utils.showSnackBar
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactFragment : Fragment() {

    private lateinit var binding: FragmentContactBinding
    private val progressBar = CustomProgressDialog()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentContactBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {

        binding.etPhone.setText(number)

        binding.btnSubmit.setOnClickListener {
            if (isValidate() && Utils.isNetworkAvailable(requireContext())) {
                saveFeedBack()
            }
        }
    }

    private fun isValidate(): Boolean {

        return when {
            binding.etName.text.toString().trim().isEmpty() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_name))
                false
            }
            binding.etEmail.text.toString().trim().isEmpty() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_email))
                false
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString().trim()).matches() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_correct_email))
                false
            }
            binding.etPhone.text.toString().trim().isEmpty() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_phone))
                false
            }
            !Patterns.PHONE.matcher(binding.etPhone.text.toString().trim()).matches() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_phone_check))
                false
            }
            binding.etPhone.text.toString().trim().length != 10 -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_phone_check))
                false
            }
            binding.etCity.text.toString().trim().isEmpty() -> {
                showSnackBar(requireContext(), getString(R.string.please_enter_city))
                false
            }
            else -> {
                true
            }
        }
    }

    private fun saveFeedBack() {

        val question = questionsId?.joinToString { it }
        val answer = answerId?.joinToString { it.toString() }

        val rate = ratingList?.sum()?.div(ratingList?.size!!)

        val login = MyPreference(requireContext()).getData("login")?.data
        val userDetails = MyPreference(requireContext()).getUserDetails("userDetails")?.data
        progressBar.show(requireContext())
        val body = MultipartBody.Builder().setType(MultipartBody.FORM)
            .addFormDataPart("branchId", "${RequestFeedBack.branchid}")
            .addFormDataPart("userId", "${login?.id}")
            .addFormDataPart("sessionId", "${RequestFeedBack.sessionid}")
            .addFormDataPart("rating", "$rate")
            .addFormDataPart("comments", "$comments")
            .addFormDataPart("Subscribed", "$subscribe")
            .addFormDataPart("questionsId", "$question")
            .addFormDataPart("answer", "$answer")
//            .addFormDataPart("customerId", "${userDetails.id}")
            .addFormDataPart("customerName", "${binding.etName.text.toString().trim()}")
            .addFormDataPart("customerEmail", "${binding.etEmail.text.toString().trim()}")
            .addFormDataPart("customerPhone", "${binding.etPhone.text.toString().trim()}")
            .addFormDataPart("customerCity", "${binding.etCity.text.toString().trim()}")

        val request = body.build()

        ServiceBuilder().service.saveFeedBack(request)
            .enqueue(
                object : Callback<ModelSaveFeedBack> {
                    override fun onResponse(
                        call: Call<ModelSaveFeedBack>,
                        response: Response<ModelSaveFeedBack>
                    ) {
                        if (response.isSuccessful && response.body()?.status == true) {
                            findNavController().navigate(R.id.thankYouFragment)
                        } else {
                            showSnackBar(requireContext(), response.body()?.message.toString())
                        }
                        progressBar.dialog.dismiss()
                    }

                    override fun onFailure(call: Call<ModelSaveFeedBack>, t: Throwable) {
                        progressBar.dialog.dismiss()
                        showSnackBar(requireContext(), t.message.toString())
                    }
                })

    }

}