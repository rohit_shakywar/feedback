package com.feedback.home.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.feedback.R
import com.feedback.databinding.FragmentStartFeedBackBinding
import com.feedback.storage.MyPreference

class StartFeedBackFragment : Fragment() {

    private lateinit var binding: FragmentStartFeedBackBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartFeedBackBinding.inflate(inflater, container, false)

        initListener()
        return binding.root
    }

    private fun initListener() {

        binding.btnGiveFeedback.setOnClickListener {

            findNavController().navigate(R.id.sessionFragment)
        }

        binding.ivLogout.setOnClickListener {

            AlertDialog.Builder(requireContext()).setTitle("Are you sure want to logout")
                .setNegativeButton("No") { dialog, which -> dialog.dismiss() }
                .setPositiveButton("Yes") { dialog, which ->
                    dialog.dismiss()
                    MyPreference(requireContext()).onLogOut(requireContext())
                }
                .show()
        }
    }

}