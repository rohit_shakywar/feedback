package com.feedback.home.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.feedback.R
import com.feedback.databinding.FragmentSelectServerBinding
import com.skydoves.powerspinner.IconSpinnerAdapter
import com.skydoves.powerspinner.IconSpinnerItem


class SelectServerFragment : Fragment() {

    private lateinit var binding: FragmentSelectServerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentSelectServerBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {

        binding.selectOption.setIsFocusable(true)
        binding.selectTable.setIsFocusable(true)

        val listOption = arrayListOf<String>(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten"
        )
        val listTable = arrayListOf<String>(
            "One",
            "Two",
            "Three",
            "Four",
            "Five",
            "Six",
            "Seven",
            "Eight",
            "Nine",
            "Ten"
        )
        binding.selectOption.apply {
            setItems(listOption)
            setOnSpinnerItemSelectedListener<String> { _, _, _, item ->
                Toast.makeText(requireContext(), item, Toast.LENGTH_SHORT).show()
            }
            getSpinnerRecyclerView().layoutManager = GridLayoutManager(requireContext(), 1)
        }
        binding.selectTable.apply {
            setItems(listTable)
            setOnSpinnerItemSelectedListener<String> { _, _, _, item ->
                Toast.makeText(requireContext(), item, Toast.LENGTH_SHORT).show()
            }
            getSpinnerRecyclerView().layoutManager = GridLayoutManager(requireContext(), 1)
        }
        binding.btnOk.setOnClickListener {
            findNavController().navigate(R.id.branchQuestionFragment)
        }
        getBranchQuestion()
    }

    private fun getBranchQuestion() {

    }

}