package com.feedback.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.feedback.R
import com.feedback.databinding.FragmentOverallServicesBinding


class OverallServicesFragment : Fragment() {

    private lateinit var binding: FragmentOverallServicesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentOverallServicesBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {


        binding.constSelection1?.setOnClickListener {

            if (binding.constSelection1?.isSelected == true) {
                binding.constSelection1?.isSelected = false
                binding.card1?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey1?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView1?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck1?.visibility = View.GONE



            } else {
                binding.constSelection1?.isSelected = true
                binding.card1?.strokeColor=ContextCompat.getColor(requireContext(),R.color.purple_500)
                binding.tvKey1?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.textView1?.setTextColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.ivCheck1?.visibility = View.VISIBLE

                binding.constSelection2?.isSelected = false
                binding.card2?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey2?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView2?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck2?.visibility = View.GONE

                binding.constSelection3?.isSelected = false
                binding.card3?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey3?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView3?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck3?.visibility = View.GONE

                binding.constSelection4?.isSelected = false
                binding.card4?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey4?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView4?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck4?.visibility = View.GONE
            }
        }

 binding.constSelection2?.setOnClickListener {

            if (binding.constSelection2?.isSelected == true) {
                binding.constSelection2?.isSelected = false
                binding.card2?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey2?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView2?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck2?.visibility = View.GONE

            } else {
                binding.constSelection2?.isSelected = true
                binding.card2?.strokeColor=ContextCompat.getColor(requireContext(),R.color.purple_500)
                binding.tvKey2?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.textView2?.setTextColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.ivCheck2?.visibility = View.VISIBLE

                binding.constSelection1?.isSelected = false
                binding.card1?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey1?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView1?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck1?.visibility = View.GONE

                binding.constSelection3?.isSelected = false
                binding.card3?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey3?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView3?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck3?.visibility = View.GONE

                binding.constSelection4?.isSelected = false
                binding.card4?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey4?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView4?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck4?.visibility = View.GONE
            }
        }

            binding.constSelection3?.setOnClickListener {

            if (binding.constSelection3?.isSelected == true) {
                binding.constSelection3?.isSelected = false
                binding.card3?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey3?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView3?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck3?.visibility = View.GONE

            } else {
                binding.constSelection3?.isSelected = true
                binding.card3?.strokeColor=ContextCompat.getColor(requireContext(),R.color.purple_500)
                binding.tvKey3?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.textView3?.setTextColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.ivCheck3?.visibility = View.VISIBLE

                binding.constSelection1?.isSelected = false
                binding.card1?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey1?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView1?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck1?.visibility = View.GONE

                binding.constSelection2?.isSelected = false
                binding.card2?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey2?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView2?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck2?.visibility = View.GONE

                binding.constSelection4?.isSelected = false
                binding.card4?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey4?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView4?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck4?.visibility = View.GONE

            }
        }
        binding.constSelection4?.setOnClickListener {

            if (binding.constSelection4?.isSelected == true) {
                binding.constSelection4?.isSelected = false
                binding.card4?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey4?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView4?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck4?.visibility = View.GONE

            } else {
                binding.constSelection4?.isSelected = true
                binding.card4?.strokeColor=ContextCompat.getColor(requireContext(),R.color.purple_500)
                binding.tvKey4?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.textView4?.setTextColor(ContextCompat.getColor(requireContext(),R.color.purple_500))
                binding.ivCheck4?.visibility = View.VISIBLE

                binding.constSelection3?.isSelected = false
                binding.card3?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey3?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView3?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck3?.visibility = View.GONE

                binding.constSelection2?.isSelected = false
                binding.card2?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey2?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView2?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck2?.visibility = View.GONE

                binding.constSelection1?.isSelected = false
                binding.card1?.strokeColor=ContextCompat.getColor(requireContext(),R.color.white)
                binding.tvKey1?.setBackgroundColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.textView1?.setTextColor(ContextCompat.getColor(requireContext(),R.color.white))
                binding.ivCheck1?.visibility = View.GONE
            }
        }



        binding.btnOk.setOnClickListener {
        }
    }
}