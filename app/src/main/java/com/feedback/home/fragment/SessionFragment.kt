package com.feedback.home.fragment

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.feedback.R
import com.feedback.databinding.FragmentSessionBinding
import com.feedback.home.adapter.AdapterSession
import com.feedback.model.ModelLogin
import com.feedback.model.RequestFeedBack.branchid
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.storage.MyPreference
import com.feedback.util.Utils.isNetworkAvailable
import com.feedback.util.Utils.showSnackBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SessionFragment : Fragment() {

    private lateinit var binding: FragmentSessionBinding
    private val progressBar = CustomProgressDialog()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSessionBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    AlertDialog.Builder(requireContext()).setTitle("Are you sure want to exit")
                        .setNegativeButton("No") { dialog, which ->
                            dialog.dismiss()
                        }
                        .setPositiveButton("Yes") { dialog, which ->
                            dialog.dismiss()
                            findNavController().popBackStack()
                        }
                        .show()
                }
            })






        branchid = null
        binding.btnNext.setOnClickListener {

            if (branchid == null) {
                showSnackBar(requireContext(), "Please select Session")
            } else {
                findNavController().navigate(R.id.homeFragment)
            }
        }

        binding.btnRefresh.setOnClickListener {
            if (isNetworkAvailable(requireContext())) {
                progressBar.show(requireContext(), getString(R.string.please_wait))
                getLoginData()
            }
        }

        initSetUpView(MyPreference(requireContext()).getData("login")!!)
    }

    private fun initSetUpView(data: ModelLogin) {

        binding.recyclerView.layoutManager = GridLayoutManager(requireContext(),3)
        binding.recyclerView.adapter =
            AdapterSession(requireContext(), data?.data?.session)

    }

    private fun getLoginData() {
        val login = MyPreference(requireContext()).getData("login")?.data
        ServiceBuilder().service.apply {
            this.getBranchLogin(login?.branch_email.toString(), login?.branch_password.toString())
                .enqueue(object : Callback<ModelLogin> {
                    override fun onResponse(
                        call: Call<ModelLogin>,
                        response: Response<ModelLogin>
                    ) {
                        if (response.isSuccessful && response.body()?.status == true) {
                            MyPreference(requireContext()).saveData("login", response?.body()!!)
                            initSetUpView(response?.body()!!)
                        } else {
                            showSnackBar(requireContext(), response?.body()?.message.toString())
                        }
                        progressBar.dialog.dismiss()
                    }

                    override fun onFailure(call: Call<ModelLogin>, t: Throwable) {
//                    Log.i("onError:", t.toString())
                        progressBar.dialog.dismiss()
                        showSnackBar(requireContext(), t.message.toString())
                    }
                })
        }

    }


}