package com.feedback.home.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import com.feedback.R
import com.feedback.databinding.FragmentThankYouBinding
import com.feedback.home.HomeActivity


class ThankYouFragment : Fragment() {
    private lateinit var binding: FragmentThankYouBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentThankYouBinding.inflate(inflater, container, false)
        initListener()
        return binding.root
    }

    private fun initListener() {

        activity?.onBackPressedDispatcher?.addCallback {
            startActivity(Intent(requireContext(), HomeActivity::class.java))
            activity?.finish()
        }
        binding.btnNext.setOnClickListener {
            startActivity(Intent(requireContext(), HomeActivity::class.java))
            activity?.finish()
        }
    }

}