package com.feedback.activity

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.feedback.model.ModelLogin
import com.feedback.R
import com.feedback.databinding.ActivityLoginBinding
import com.feedback.home.HomeActivity
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.storage.MyPreference
import com.feedback.util.Utils
import com.feedback.util.Utils.hideSoftKeyboard
import com.feedback.util.Utils.showSnackBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val progressBar = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initListener()
    }

    private fun initListener() {

        binding.etEmail.addTextChangedListener {
            if (it?.isNotEmpty()!!) {
                binding.tilEmail.error = null
            }
        }
        binding.etPassword.addTextChangedListener {
            if (it?.isNotEmpty()!!) {
                binding.tilPassword.error = null
            }
        }


        binding.tvForgot.setOnClickListener {
            startActivity(Intent(this, ForgotPasswordActivity::class.java))
        }
        binding.btnLogin.setOnClickListener {

            if (isValidate() && Utils.isNetworkAvailable(this)) {

                progressBar.show(this, getString(R.string.please_wait))
                login(
                    binding.etEmail.text.toString().trim(),
                    binding.etPassword.text.toString().trim()
                )

            }
        }

    }

    private fun login(username: String, password: String) {

        ServiceBuilder().service.apply {
            this.getBranchLogin(username, password).enqueue(object : Callback<ModelLogin> {
                override fun onResponse(
                    call: Call<ModelLogin>,
                    response: Response<ModelLogin>
                ) {
                    if (response.isSuccessful && response.body()?.status == true) {
                        MyPreference(this@LoginActivity).saveData("login", response?.body()!!)
//                        Log.i("onResponse:", response.toString())
                        startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                        finish()
                    } else {
                        showSnackBar(this@LoginActivity, response?.body()?.message.toString())
                    }
                    progressBar.dialog.dismiss()
                }

                override fun onFailure(call: Call<ModelLogin>, t: Throwable) {
//                    Log.i("onError:", t.toString())
                    progressBar.dialog.dismiss()
                    showSnackBar(this@LoginActivity, t.message.toString())
                }
            })
        }

    }

    private fun isValidate(): Boolean {
        return when {
            binding.etEmail.text.toString().trim().isEmpty() -> {
                binding.tilEmail.requestFocus()
                binding.tilEmail.error = getString(R.string.please_enter_email)
                false
            }
            !Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString().trim()).matches() -> {
                binding.tilEmail.requestFocus()
                binding.tilEmail.error = getString(R.string.please_enter_correct_email)
                false
            }

            binding.etPassword.text.toString().trim().isEmpty() -> {
                binding.tilPassword.requestFocus()
                binding.tilPassword.error = getString(R.string.please_enter_password)
                false
            }


            else -> {
                true
            }

        }

    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            hideSoftKeyboard(this)
        }
        return super.dispatchTouchEvent(ev)
    }
}