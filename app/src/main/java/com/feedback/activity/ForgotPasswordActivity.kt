package com.feedback.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Patterns
import android.view.MotionEvent
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import com.feedback.R
import com.feedback.databinding.ActivityForgotPasswordBinding
import com.feedback.model.ModelSaveFeedBack
import com.feedback.networking.ServiceBuilder
import com.feedback.progressbar.CustomProgressDialog
import com.feedback.util.Utils
import com.feedback.util.Utils.hideSoftKeyboard
import com.feedback.util.Utils.showSnackBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : AppCompatActivity() {
    private lateinit var binding: ActivityForgotPasswordBinding
    private val progressBar = CustomProgressDialog()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initListener()
    }

    private fun initListener() {

        binding.etEmail.addTextChangedListener {
            if (it?.isNotEmpty()!!) {
                binding.tilEmail.error = null
            }
        }

        binding.btnSubmit.setOnClickListener {

            when {
                binding.etEmail.text.toString().trim().isEmpty() -> {
                    binding.tilEmail.requestFocus()
                    binding.tilEmail.error = getString(R.string.please_enter_email)
                    return@setOnClickListener
                }

                !Patterns.EMAIL_ADDRESS.matcher(binding.etEmail.text.toString().trim())
                    .matches() -> {
                    binding.tilEmail.requestFocus()
                    binding.tilEmail.error = getString(R.string.please_enter_correct_email)
                    return@setOnClickListener
                }
                else -> {
                    forgetPassword()
                }
            }

        }

    }

    private fun forgetPassword() {
        progressBar.show(this, getString(R.string.please_wait))

        ServiceBuilder().service.forgotPassword(binding.etEmail.text.toString().trim())
            .enqueue(object : Callback<ModelSaveFeedBack> {
                override fun onResponse(
                    call: Call<ModelSaveFeedBack>,
                    response: Response<ModelSaveFeedBack>
                ) {
                    if (response.isSuccessful && response?.body()?.status == true) {
                        progressBar.dialog.dismiss()
                        showSnackBar(
                            this@ForgotPasswordActivity,
                            response?.body()?.message.toString()
                        )
                        Handler(Looper.getMainLooper()).postDelayed({
                            finish()
                        }, 2000)

                    } else {
                        progressBar.dialog.dismiss()

                        showSnackBar(
                            this@ForgotPasswordActivity,
                            response?.body()?.message.toString()
                        )
                    }
                }

                override fun onFailure(call: Call<ModelSaveFeedBack>, t: Throwable) {
                    progressBar.dialog.dismiss()
                    showSnackBar(this@ForgotPasswordActivity, t.message.toString())
                }
            })
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {

        if (currentFocus != null) {
            hideSoftKeyboard(this)
        }

        return super.dispatchTouchEvent(ev)
    }
}