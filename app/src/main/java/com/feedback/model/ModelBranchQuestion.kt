package com.feedback.model

 data class ModelBranchQuestion(
    val message: String,
    val status: Boolean,
    val `data`: ArrayList<DataBranch>
 )

data class DataBranch(
    val branchId: String,
    val createdAt: String,
    val id: String,
    val isDefault: String,
    val questionType: String,
    val templateId: String,
    val title: String
)