package com.feedback.model

object RequestFeedBack {

    var branchid:String?=null
    var sessionid:String?=null
    var comments:String?=null
    var number:String?=null
    var subscribe:String?=null
    var questionsId:ArrayList<String>?=null
    var answerId:ArrayList<Int>?=null
    var ratingList:ArrayList<Int>?=null
}