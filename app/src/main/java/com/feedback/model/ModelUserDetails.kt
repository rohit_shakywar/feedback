package com.feedback.model
import com.google.gson.annotations.SerializedName



 data class ModelUserDetails(
    val `data`: DataUser,
    val message: String, // user Detail
    val status: Boolean // true
)

data class DataUser(
    val city: String, // Tohana
    val createdAt: String, // 2021-03-27 03:34:09
    val email: String, // rahulmehndiratta@live.com
    val id: String, // 1748
    val name: String, // Rahul
    val phone: String, // 9068906850
    val updatedAt: String // 2021-03-27 03:34:09
)