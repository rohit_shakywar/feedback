package com.feedback.model

 data class ModelLogin(
    val `data`: DataLogin,
    val message: String,
    val status: Boolean
)

data class DataLogin(
    val branch_address: String,
    val branch_email: String,
    val branch_name: String,
    val branch_password: String,
    val branch_phone: String,
    val created_at: String,
    val deleted_at: Any,
    val id: String,
    val image: String,
    val restaurant_id: String,
    val session: ArrayList<Session>,
    val updated_at: String
)

data class Session(
    val branchId: String,
    val createdAt: String,
    val questions: ArrayList<Question>,
    val sessionId: String,
    val sessionName: String,
    val updatedAt: String
)

data class Question(
    val branchId: String,
    val createdAt: String,
    val id: String,
    val isDefault: String,
    val questionId: String,
    val questionType: String,
    val sessionId: String,
    val templateId: String,
    val title: String
)