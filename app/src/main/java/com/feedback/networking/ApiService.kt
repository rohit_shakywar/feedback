package com.feedback.networking

import com.feedback.model.ModelBranchQuestion
import com.feedback.model.ModelLogin
import com.feedback.model.ModelSaveFeedBack
import com.feedback.model.ModelUserDetails
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {
    @FormUrlEncoded
    @POST("getBranchLogin")
    fun getBranchLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Call<ModelLogin>


    @FormUrlEncoded
    @POST("getBranchQuestion")
    fun getBranchQuestion(
        @Field("branchId") branchId: String
    ): Call<ModelBranchQuestion>


    @FormUrlEncoded
    @POST("getUserDetail")
    fun getUserDetail(
        @Field("phone") phone: String
    ): Call<ModelUserDetails>


    @FormUrlEncoded
    @POST("saveUserDetail")
    fun saveUserDetail(
        @Field("email") email: String,
        @Field("phone") phone: String,
        @Field("name") name: String,
        @Field("city") city: String
    ): Call<ResponseBody>

    @POST("saveFeedBack")
    fun saveFeedBack(@Body request: RequestBody): Call<ModelSaveFeedBack>

    @FormUrlEncoded
    @POST("forgot_password")
    fun forgotPassword(
        @Field("email") email: String,
    ): Call<ModelSaveFeedBack>

}