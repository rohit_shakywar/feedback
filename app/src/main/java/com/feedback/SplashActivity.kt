package com.feedback

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.feedback.activity.LoginActivity
import com.feedback.home.HomeActivity
import com.feedback.storage.MyPreference
import com.feedback.util.Utils

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Log.d( "Height:",Utils.getHeight().toString())
        Log.d( "Width:",Utils.getWidth().toString())

        val login = MyPreference(this).getData("login")?.data
        Handler(Looper.getMainLooper()).postDelayed({

            when {
                login != null && login.id.isNotEmpty() -> {
                    startActivity(Intent(this, HomeActivity::class.java))
                    finish()
                }
                else -> {
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
            }

        }, 2000)
    }
}