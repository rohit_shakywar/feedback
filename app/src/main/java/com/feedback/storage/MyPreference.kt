package com.feedback.storage

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.feedback.model.ModelLogin
import com.feedback.R
import com.feedback.activity.LoginActivity
import com.feedback.model.ModelUserDetails
import com.google.gson.Gson


class MyPreference(val context: Context) {

    val sharedPref = context.getSharedPreferences(
        context.getString(R.string.app_name),
        Context.MODE_PRIVATE
    )


    fun saveData(keyName: String, value: ModelLogin) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(keyName, Gson().toJson(value))
        editor.apply()
    }

    fun getData(keyName: String): ModelLogin? {
        val gson = Gson()
        val json = sharedPref.getString(keyName, null)
        return gson.fromJson(json, ModelLogin::class.java)
    }

    fun saveUserDetails(keyName: String, value: ModelUserDetails?) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString(keyName, Gson().toJson(value))
        editor.apply()
    }

    fun getUserDetails(keyName: String): ModelUserDetails {
        val gson = Gson()
        val json = sharedPref.getString(keyName, null)
        return gson.fromJson(json, ModelUserDetails::class.java)
    }

    fun onLogOut(context: Context) {
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.clear()
        editor.apply()
        val intent = Intent(context, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        context.startActivity(intent)
    }

}